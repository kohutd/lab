#include <stdio.h>
#include <math.h>

int main() {
	double start, end;
	double step;

	printf("start = ");
	scanf("%lf", &start);

	printf("end = ");
	scanf("%lf", &end);

	printf("step = ");
	scanf("%lf", &step);

	printf("---------------------\n");
	printf("  C  	  F  \n");
	printf("---------------------\n");

	while (start <= end) {
		double Tf = 1.8 * start + 32;
		printf("%.2lf	%.2lf\n", start, Tf);

		start += step;
	}

	return 0;
}
