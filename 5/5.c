#include <stdio.h>

void sortArray(int [], int);

int main() {
	int firstSize;
	printf("Enter size of first array: \n");
	scanf("%d", &firstSize);
	int first[firstSize];
	for (int i = 0; i < firstSize; ++i) {
		printf("element %d of first array = ", i + 1);
		scanf("%d", &first[i]);
	}

	int secondSize;
	printf("Enter size of second array: \n");
	scanf("%d", &secondSize);
	int second[secondSize];
	for (int i = 0; i < secondSize; ++i) {
		printf("element %d of second array = ", i + 1);
		scanf("%d", &second[i]);
	}


	int newSize = firstSize + secondSize;
	int newArr[newSize];

	int sNext = 0;

	for (int i = 0; i < newSize; ++i) {
		if (i < firstSize) {
			newArr[i] = first[i];
		} else {
			newArr[i] = second[sNext];
			sNext++;
		}
	}
	
	sortArray(newArr, newSize);

	printf("\n");
	printf("New: \n");

	for (int i = 0; i < newSize; ++i) {
		printf("%d ", newArr[i]);
	}

	printf("\n");
}


void sortArray(int arr[], int size) {
	for (int i = 0; i < size; ++i) {
		for (int j = i + 1; j < size; ++j) {
			if (arr[j] < arr[i]) {
				int tmp = arr[i];
				arr[i] = arr[j];
				arr[j] = tmp;
			}
		}
	}
}