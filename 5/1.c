#include <stdio.h>

int main() {
	double arr[7];
	double sum = 0;

	for (int i = 0; i < 7; ++i) {
		printf("Write a %d number: \n", i + 1);
		scanf("%lf", &arr[i]);
	}

	for (int i = 0; i < 7; ++i) {
		sum += arr[i];
	}

	printf("Avg: %lf\n", sum / 7);
}
