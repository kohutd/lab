#include <stdio.h>
#include <string.h>

int main() {
	char first[1000];
	char second[1000];

	printf("Enter a first string: \n");
	fgets(first, 1000, stdin);

	printf("Enter a second string: \n");
	fgets(second, 1000, stdin);

	int flen = strlen(first) - 1;
	int slen = strlen(second) - 1;

	printf("\n");

	if (flen == slen) {
		for (int i = 0; i < flen; ++i) {
			if (first[i] != second[i]) {
				printf("The strings are not similar\n");
				return 0;
			}
		}
		printf("The strings are similar\n");
	} else {
		printf("%s > %s\n", (flen > slen ? "first" : "second"), (flen < slen ? "first" : "second"));
	}
}