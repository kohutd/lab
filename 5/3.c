#include <stdio.h>

int main() {
	int size;

	printf("Enter size of array: \n");
	scanf("%d", &size);

	int arr[size];

	for (int i = 0; i < size; ++i) {
		printf("element %d = ", i + 1);
		scanf("%d", &arr[i]);
	}


	int newArr[size + 1];

	int newElement = 0;
	printf("Enter new element value: \n");
	scanf("%d", &newElement);

	int newElementPosition = 0;
	printf("Enter new element position: \n");
	scanf("%d", &newElementPosition);

	for (int i = 0; i < size + 1; ++i) {
		if (i == newElementPosition - 1) {
			newArr[i] = newElement;
		} else if (i > newElementPosition - 1) {
			newArr[i] = arr[i - 1];
		} else {
			newArr[i] = arr[i];
		}
	}

	printf("\n");
	printf("Old: \n");

	for (int i = 0; i < size; ++i) {
		printf("%d ", arr[i]);
	}

	printf("\n");
	printf("New: \n");

	for (int i = 0; i < size + 1; ++i) {
		printf("%d ", newArr[i]);
	}
	printf("\n");
}
