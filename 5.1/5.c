#include <stdio.h>

int main() {
	int row;
	int col;

	printf("Rows: ");
	scanf("%d", &row);

	printf("Cols: ");
	scanf("%d", &col);

	int matrix[row][col];

	for (int i = 0; i < row; ++i) {
		for (int j = 0; j < col; ++j) {
			printf("element of matrix [%d][%d] = ", i, j);
			scanf("%d", &matrix[i][j]);
		}
	}

	printf("Matrix: \n");
	for (int i = 0; i < row; ++i) {
		for (int j = 0; j < col; ++j) {
			printf("%d\t", matrix[i][j]);
		}
		printf("\n");
	}

	printf("\n");
	printf("Sum od rows: \n");
	for (int i = 0; i < row; ++i) {
		int sum = 0;
		for (int j = 0; j < col; ++j) {
			sum += matrix[i][j];
		}

		printf("%d\n", sum);
	}


	printf("\n");
	printf("Sum od cols: \n");
	for (int i = 0; i < col; ++i) {
		int sum = 0;
		for (int j = 0; j < row; ++j) {
			sum += matrix[j][i];
		}

		printf("%d ", sum);
	}

	printf("\n");

}
