#include <stdio.h>

int main() {
	int rowFirst;
	int colFirst;
	printf("Rows of first matrix: ");
	scanf("%d", &rowFirst);
	printf("Cols of first matrix: ");
	scanf("%d", &colFirst);

	int rowSecond;
	int colSecond;
	printf("Rows of second matrix: ");
	scanf("%d", &rowSecond);
	printf("Cols of second matrix: ");
	scanf("%d", &colSecond);

	if (colFirst != rowSecond) {
		printf("The matrixes cannot be multiplied.\n");
		return 0;
	}

	int first[rowFirst][colFirst];
	int second[rowSecond][colSecond];

	// scan first matrix
	for (int i = 0; i < rowFirst; ++i) {
		for (int j = 0; j < colFirst; ++j) {
			printf("element of first matrix [%d][%d] = ", i, j);
			scanf("%d", &first[i][j]);
		}
	}

	// scan second matrix
	for (int i = 0; i < rowSecond; ++i) {
		for (int j = 0; j < colSecond; ++j) {
			printf("element of second matrix [%d][%d] = ", i, j);
			scanf("%d", &second[i][j]);
		}
	}

	printf("First matrix: \n");
	for (int i = 0; i < rowFirst; ++i) {
		for (int j = 0; j < colFirst; ++j) {
			printf("%d\t", first[i][j]);
		}
		printf("\n");
	}

	printf("Second matrix: \n");
	for (int i = 0; i < rowSecond; ++i) {
		for (int j = 0; j < colSecond; ++j) {
			printf("%d\t", second[i][j]);
		}
		printf("\n");
	}

	int multiplied[rowFirst][colSecond];

	for (int i = 0; i < rowFirst; ++i) {
		for (int j = 0; j < colSecond; ++j) {
			int sum = 0;

			for (int k = 0; k < rowSecond; ++k) {
				sum += (first[i][k] * second[k][j]);
			}

			multiplied[i][j] = sum;
		}
	}

	printf("\n");
	printf("Result: \n");
	for (int i = 0; i < rowFirst; ++i) {
		for (int j = 0; j < colSecond; ++j) {
			printf("%d\t", multiplied[i][j]);
		}
		printf("\n");
	}
}
