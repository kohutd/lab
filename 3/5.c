#include <stdio.h>
#include <math.h>

int main() {
	double a, b, c;

	printf("A: ");
	scanf("%lf", &a);

	printf("B: ");
	scanf("%lf", &b);

	printf("C: ");
	scanf("%lf", &c);

	if (
		(a + b) > c ||
		(a + c) > b ||
		(b + c) > a
	) {
		double p = a + b + c;
		printf("P: %.1lf\n", p);
	}

	return 0;
}
