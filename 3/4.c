#include <stdio.h>

int main() {
	int sum = 0;

	for (int i = 0; i < 5; i++) {
		int tmp = 0;

		printf("number_%d = ", i + 1);
		scanf("%d", &tmp);

		if (tmp % 2 != 0) {
			sum += tmp;
		}
	}

	printf("\nsum = %d\n", sum);

	return 0;
}

/* #include <stdio.h>

int main() {
	int a[5];
	int sum = 0;

	for (int i = 0; i < 5; i++) {
		printf("number_%d = ", i + 1);
		scanf("%d", &a[i]);
	}

	for (int i = 0; i < 5; i++) {
		if (a[i] % 2 != 0) {
			sum += a[i];
		}
	}

	printf("\nsum = %d\n", sum);

	return 0;
}*/
