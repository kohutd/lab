#include <stdio.h>

int main() {
	int p, q, r, s;

	printf("P: ");
	scanf("%d", &p);

	printf("Q: ");
	scanf("%d", &q);

	printf("R: ");
	scanf("%d", &r);

	printf("S: ");
	scanf("%d", &s);

	if ((q > r) && (s > p) && ((r + s) > (p + q)) && (r > 0) && (s > 0) && (p % 2 == 0)) {
		printf("Correct.\n");
	} else {
		printf("Wrong!");
	}

	return 0;
}
