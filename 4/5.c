#include <stdio.h>
#include <math.h>

int main() {
	unsigned int number = 0;

	do {
		printf("Type a number from 1 to 100: ");
		scanf("%d", &number);
	} while (number < 1 || number > 100);


	printf("\n");
	printf("Result: \n");
	
	for (int i = 1; i <= 500; i++) {
		if (i % number == 3) {
			printf("%d ", i);
		}
	}

	printf("\n");
}
