#include <stdio.h>

int main() {
	char id[10];
	int workHours, perHour;

	printf("ID: ");
	scanf("%s", &id);

	printf("Work hours: ");
	scanf("%d", &workHours);

	printf("UAH per hour: ");
	scanf("%d", &perHour);

	float perMonth = (float) perHour * (float) workHours * 20;

	printf("-----\n");
	printf("ID: %s\n", id);
	printf("Per month: %.2f\n", perMonth);

}
